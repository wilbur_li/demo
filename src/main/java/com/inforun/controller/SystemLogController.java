package com.inforun.controller;

import com.github.pagehelper.PageHelper;
import com.inforun.model.SystemLog;
import com.inforun.service.SystemLogService;
import com.inforun.service.redis.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("systemlog")
@RestController
public class SystemLogController {
    private static Logger log = LoggerFactory.getLogger(SystemLogController.class);

    @Autowired
    SystemLogService systemLogService;

    @Autowired
    RedisService redisService;

    @RequestMapping(value = "/getById")
    @ResponseBody
    public SystemLog getById(){
        SystemLog systemLog = systemLogService.byId(474351);
        log.info("=========this is my testlog==========="+systemLog.getLogTime());
        // 测试ehcache有没有工作,取的是缓存里面的值，一般用来存储不变或很少变动的数据
        SystemLog systemLog2 = systemLogService.byId(474351);
        log.info("=========this is my testlog2==========="+systemLog.getLogTime());
        return systemLog;
    }

    @RequestMapping(value = "/addRedisData")
    @ResponseBody
    public String addRedisData(){

        String key = "redisTest";
        String value = "{test:1}";
        redisService.set(key,value);
        return "Saved Success";
    }

    @RequestMapping(value = "/getAllSystemLog")
    @ResponseBody
    public List<SystemLog> getAllSystemLog(){
        log.info("=========this is my testlog===========");
        PageHelper.startPage(1,20);
        List<SystemLog> list = systemLogService.getAll();
//        PageInfo<SystemLog> pageInfo = new PageInfo<SystemLog>(list);
        return  list;

    }

}
