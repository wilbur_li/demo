package com.inforun.config;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.cache.CacheManager;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.Duration;
import javax.cache.expiry.TouchedExpiryPolicy;
import java.util.concurrent.TimeUnit;

/**
 * @Author: wilburli
 * @Description:
 * @Date: Created by sh on 2017-10-16.
 */
@Configuration
@EnableCaching
@Component
public class Ehcache3Config implements JCacheManagerCustomizer {

    private static final String NAME_CACHE = "systemlog";

    @Override
    public void customize(CacheManager cacheManager) {
        cacheManager.createCache(NAME_CACHE,
                new MutableConfiguration<>()
                        .setExpiryPolicyFactory(TouchedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS, 10)))
                        .setStoreByValue(true).setStatisticsEnabled(true));
    }

}
