package com.inforun.service.impl;

import com.inforun.dao.SystemLogMapper;
import com.inforun.model.SystemLog;
import com.inforun.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SystemLogServiceImpl implements SystemLogService {
    public static final String CACHE_NAME = "systemlog";

    public static final String THING_ALL_KEY  = "'systemlog_all'";

    @Autowired
    SystemLogMapper systemLogDao;

    @Override
    @Cacheable(value = CACHE_NAME,key = THING_ALL_KEY)
    public SystemLog byId(Integer id) {
        return systemLogDao.selectByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public List<SystemLog> getAll() {
        return systemLogDao.getAll();
    }


}
