package com.inforun.service;

import com.inforun.model.SystemLog;

import java.util.List;

public interface SystemLogService {
    SystemLog byId(Integer id);
    List<SystemLog> getAll();
}
